from flask import Flask, render_template, request, redirect, url_for
from flask_sqlalchemy import SQLAlchemy, SignallingSession
import io
import csv
#create app's globals
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////Users/AdamGoldberg2/Desktop/CS564/stage5/src/data.db'
db = SQLAlchemy(app)

#todo: use dropdowns for search values
#todo: within each individual page, allow search for just that table
''' Create all tables as models from our connected data.db'''

class Character(db.Model):
	__tablename__ = 'Character'
	CharID = db.Column('CharID', db.String(30), primary_key = True)
	Gender = db.Column('Gender', db.String(7))

	def __init__(self, cid, gender):
		self.CharID = cid
		self.Gender = gender

class Console(db.Model):
	__tablename__ = 'Console'
	ConsID = db.Column('ConsID', db.String(30), primary_key = True)
	CompanyID = db.Column('CompanyID', db.String(30))

	def __init__(self, cid, coid):
		self.ConsID = cid
		self.CompanyID = coid

class Critic(db.Model):
	__tablename__ = 'Critic'
	CrID = db.Column('CrID', db.String(50), primary_key = True)
	DOB = db.Column('DOB', db.String(20))

	def __init__(self, crid, dob):
		self.CrID = crid
		self.DOB = dob

class HardwareCompany(db.Model):
	__tablename__ = 'HardwareCompany'
	CompanyID = db.Column('CompanyID', db.String(30), primary_key = True)
	Location = db.Column('Location', db.String(100))
	DateEstablished = db.Column('DateEstablished', db.Integer())

	def __init__(self, coid, loc, deb):
		self.CompanyID = coid
		self.Location = loc
		self.DateEstablished = deb

class SoftwareCompany(db.Model):
	__tablename__ = 'SoftwareCompany'
	CompanyID = db.Column('CompanyID', db.String(30), primary_key = True)
	Location = db.Column('Location', db.String(100))
	DateEstablished = db.Column('DateEstablished', db.String(20))

	def __init__(self, coid, loc, deb):
		self.CompanyID = coid
		self.Location = loc
		self.DateEstablished = deb

class Review(db.Model):
	__tablename__ = 'Review'
	CrID = db.Column('CrID', db.String(30), primary_key = True)
	VID = db.Column('VID', db.Integer())
	Date = db.Column('Date', db.String(20))
	Rating = db.Column('Rating', db.Float())

	def __init__(self, crid, vid, date, rating):
		self.CrID = crid
		self.VID = vid
		self.Date = date
		self.Rating = rating

class VideoGameName(db.Model):
	__tablename__ = 'VideoGameName'
	VID = db.Column('VID', db.Integer(), primary_key = True)
	Name = db.Column('Name', db.String(30))

	def __init__(self, vid, name):
		self.VID = vid
		self.Name = name

class VideoGamePlatform(db.Model):
	__tablename__ = 'VideoGamePlatform'
	VID = db.Column('VID', db.Integer(), primary_key = True)
	Platforms = db.Column('Platforms', db.String(100))
	ReleaseDT = db.Column('ReleaseDT', db.String(20))
	CopiesSold = db.Column('CopiesSold', db.Integer())

	def __init__(self, vid, pform, rdt, cs):
		self.VID = vid
		self.Platforms = pform
		self.ReleaseDT = rdt
		self.CopiesSold = cs

@app.route('/')
def index():
	return render_template('home.html')

@app.route('/games')
def games_page():
	return render_template('games.html', games = VideoGameName.query.all(), platforms = VideoGamePlatform.query.all())

@app.route('/games/add', methods = ['POST'])
def add_game():
	game = VideoGameName(request.form['VID'], request.form['Name'])
	platform = VideoGamePlatform(request.form['VID'], request.form['Platforms'], request.form['ReleaseDT'], request.form['CopiesSold'])
	db.session.add(game)
	db.session.commit()
	db.session.add(platform)
	db.session.commit()
	return redirect(url_for('games_page'))

@app.route('/games/add_games_bulk', methods = ['POST'])
def add_games_bulk():
	#data_file
	with open(request.form['data_file'], 'rb') as csvfile:
		reader = csv.reader(csvfile, delimiter=',')
		for row in reader:
			game = VideoGameName(row[0], row[1])
			db.session.add(game)
			db.session.commit()

	return redirect(url_for('games_page'))

@app.route('/games/add_platforms_bulk', methods = ['POST'])
def add_platforms_bulk():
	#data_file
	with open(request.form['data_file'], 'rb') as csvfile:
		reader = csv.reader(csvfile, delimiter='|')
		for row in reader:
			platform = VideoGamePlatform(row[0], row[1], row[2], row[3])
			db.session.add(platform)
			db.session.commit()

	return redirect(url_for('games_page'))

@app.route('/games/search', methods = ['POST'])
def search_games():
	name = request.form['Name'] 
	#query all matching names
	games1 = VideoGameName.query.filter(VideoGameName.Name.contains(name)).all()
	platforms1 = []
	for game in games1:
		platforms1.append(VideoGamePlatform.query.filter(VideoGamePlatform.VID == game.VID).first())

	return render_template('games.html', games = games1, platforms = platforms1)

@app.route('/consoles')
def consoles_page():
	return render_template('consoles.html', consoles = Console.query.all(), hardCos = HardwareCompany.query.all())

@app.route('/consoles/add_console', methods = ['POST'])
def add_console():
	console = Console(request.form['cid'], request.form['company'])
	db.session.add(console)
	db.session.commit()
	return redirect(url_for('consoles_page'))

@app.route('/consoles/add_console_bulk', methods = ['POST'])
def add_console_bulk():
	#data_file
	with open(request.form['data_file'], 'rb') as csvfile:
		reader = csv.reader(csvfile, delimiter=',')
		for row in reader:
			console = Console(row[0], row[1])
			db.session.add(console)
			db.session.commit()

	return redirect(url_for('consoles_page'))

@app.route('/consoles/add_hardware', methods = ['POST'])
def add_hardware():
	developer = HardwareCompany(request.form['name'], request.form['location'], request.form['year'])
	db.session.add(developer)
	db.session.commit()
	return redirect(url_for('consoles_page'))

@app.route('/consoles/add_hardware_bulk', methods = ['POST'])
def add_hardware_bulk():
	#data_file
	with open(request.form['data_file'], 'rb') as csvfile:
		reader = csv.reader(csvfile, delimiter=',')
		for row in reader:
			hardCo = HardwareCompany(row[0], row[1], row[2])
			db.session.add(hardCo)
			db.session.commit()

	return redirect(url_for('consoles_page'))

@app.route('/consoles/search_consoles', methods = ['POST'])
def search_consoles():
	console = request.form['console']
	if console != '':
		return render_template('consoles.html', consoles = Console.query.filter(Console.ConsID.contains(console)), hardCos = HardwareCompany.query.all())

	company = request.form['company']
	consoles = Console.query.filter(Console.CompanyID.contains(company))
	hardcos = HardwareCompany.query.filter(HardwareCompany.CompanyID.contains(company))
	return render_template('consoles.html', consoles = consoles, hardCos = hardcos)

#console
#company
@app.route('/characters')
def characters_page():
	return render_template('characters.html', characters = Character.query.all())

@app.route('/characters/add', methods = ['POST'])
def add_character():
	character = Character(request.form['CID'], request.form['gender'])
	db.session.add(character)
	db.session.commit()
	return redirect(url_for('characters_page'))

@app.route('/characters/search', methods = ['POST'])
def search_characters():
	name = request.form['CID']
	return render_template('characters.html', characters = Character.query.filter(Character.CharID.contains(name)))

@app.route('/characters/add_character_bulk', methods = ['POST'])
def add_character_bulk():
	#data_file
	with open(request.form['data_file'], 'rb') as csvfile:
		reader = csv.reader(csvfile, delimiter=',')
		for row in reader:
			character = Character(row[0], row[1])
			db.session.add(character)
			db.session.commit()

	return redirect(url_for('characters_page'))

@app.route('/developers')
def developers_page():
	return render_template('developers.html', softCos = SoftwareCompany.query.all())

@app.route('/developers/add', methods = ['POST'])
def add_developer():
	developer = SoftwareCompany(request.form['name'], request.form['location'], request.form['year'])
	db.session.add(developer)
	db.session.commit()
	return redirect(url_for('developers_page'))

@app.route('/developers/add_developer_bulk', methods = ['POST'])
def add_developers_bulk():
	#data_file
	with open(request.form['data_file'], 'rb') as csvfile:
		reader = csv.reader(csvfile, delimiter=',')
		for row in reader:
			dev = SoftwareCompany(row[0], row[1], row[2])
			db.session.add(dev)
			db.session.commit()

	return redirect(url_for('characters_page'))

@app.route('/developers/search', methods = ['POST'])
def search_developers():
	name = request.form['company']
	return render_template('developers.html', softCos = SoftwareCompany.query.filter(SoftwareCompany.CompanyID.contains(name)))
