Istallation Requirements:

pip install SQLAlchemy
pip install flask

How to run the app:


change the following line in homepage.py:

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////absolute/path/to/data.db'


where the absolute/path.. is the absolute path in your computer to the db file


export FLASK_APP=homepage.py
flask run

go to browser, type into the url bar: localhost:5000


Issue that kept coming up: data.db was "locked"

How to solve the issue: run "fuser data.db"

If there is a process number that comes up, kill that process
